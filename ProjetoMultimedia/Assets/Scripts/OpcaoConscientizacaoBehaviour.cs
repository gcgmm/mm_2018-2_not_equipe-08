﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpcaoConscientizacaoBehaviour : MonoBehaviour
{
    GerenciadorPlayer gerPlayer;
    GerenciadorPlayer GerPlayer
    {
        get
        {
            if (gerPlayer == null)
                gerPlayer = transform.parent.GetComponentInParent<GerenciadorPlayer>();

            return gerPlayer;
        }
    }


    void Start()
    {
	}
	
	void Update()
    {
	}

    public void trataOnClick()
    {
        GerPlayer.ProximoQuiz(gameObject == GerPlayer.EscolhaCorreta.gameObject);
    }
}
