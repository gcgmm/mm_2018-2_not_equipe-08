﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GerenciadorCena : MonoBehaviour {

    public GameObject pCreditos;
    public GameObject bCicloAgua;
    public GameObject bConscientizacao;
    public GameObject bCreditos;
    public GameObject bSair;

    public void Sair()
    {
        Debug.Log("quit");
        Application.Quit();

    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Debug.Log("Restart");
    }

    public void CicloAgua()
    {
        Debug.Log("Carrega jogo Ciclo Água");
        SceneManager.LoadScene("CicloAgua");
    }

    public void Conscientizacao()
    {
        Debug.Log("Carrega jogo Conscientização");
        SceneManager.LoadScene("Conscientizacao");
    }

    public void Creditos()
    {
        Debug.Log("Carrega creditos");
        bCicloAgua.SetActive(false);
        bConscientizacao.SetActive(false);
        bSair.SetActive(false);
        bCreditos.SetActive(false);
        pCreditos.SetActive(true);
    }

    public void fechaCreditos()
    {
        Debug.Log("Carrega creditos");
        bCicloAgua.SetActive(true);
        bConscientizacao.SetActive(true);
        bSair.SetActive(true);
        bCreditos.SetActive(true);
        pCreditos.SetActive(false);
    }

    public void Menu()
    {
        Debug.Log("Carrega Menu");
        SceneManager.LoadScene("Menu");
    }


}
