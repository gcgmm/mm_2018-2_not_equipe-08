﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GerenciadorJogo : MonoBehaviour
{
    public const string TAG_FUNDO = "Fundo", SCRIPT_GER_JOGO = "GerenciadorJogo";
    bool podeMostrarResultado;
    public const int TOTAL_LACUNAS = 11;
    public int qtdAcertos = 0;
    public int qtdeAcessos = 0;
    public int pontuacaoFinal = -1;
    
    readonly object syncLock = new object();

    public delegate void MostrarResultadoAction(bool podeMostrar);

    public event MostrarResultadoAction OnTrocouMostrarResultado;

    public void SetPodeMostrarResultado(bool mostrarResultado)
    {
        bool mudou = podeMostrarResultado != mostrarResultado;
        podeMostrarResultado = mostrarResultado;

        if (mudou)
        {
            if (OnTrocouMostrarResultado != null)
                OnTrocouMostrarResultado(mostrarResultado);

            podeMostrarResultado = false;
        }
    }

    public void IncrementaAcessos()
    {
        qtdeAcessos++;
    }

    public void IncrementaAcertos()
    {
        lock (syncLock)
        {
            qtdAcertos++;

            //if (qtdAcertos >= TOTAL_LACUNAS && pontuacaoFinal < 0)
                //AcertouTodos();
        }
    }

    public void AcertouTodos()
    {
        Debug.Log("Vc eh o bixao");

        var timer = GameObject.Find("Canvas/TimerText").GetComponent<Timer>();
        timer.endGame();

    }

    public bool GetPodeMostrarResultado()
    {
        return podeMostrarResultado;
    }

	void Start()
    {
        podeMostrarResultado = false;
        qtdeAcessos = 0;
	}
	
	void Update()
    {
	}
}
