﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Validar : MonoBehaviour
{
    private GerenciadorJogo gerJogo;

    public void validar()
    {
        if (gerJogo == null)
            gerJogo = GameObject.FindWithTag(GerenciadorJogo.TAG_FUNDO).GetComponent(GerenciadorJogo.SCRIPT_GER_JOGO) as GerenciadorJogo;

        Debug.Log("Validar");
        gerJogo.SetPodeMostrarResultado(true);
        if (gerJogo.qtdeAcessos >= (GerenciadorJogo.TOTAL_LACUNAS - 1) && gerJogo.pontuacaoFinal < 0)
        {
            gerJogo.pontuacaoFinal = gerJogo.qtdAcertos;
            Debug.Log("Quantidade Acertos: " + gerJogo.qtdAcertos);
            Debug.Log("Quantidade Acessos: " + gerJogo.qtdeAcessos);
            
        }

        if(gerJogo.qtdAcertos >= GerenciadorJogo.TOTAL_LACUNAS)
        {
            gerJogo.AcertouTodos();
        }
    }
}
