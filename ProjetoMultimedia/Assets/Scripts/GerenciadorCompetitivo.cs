﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GerenciadorCompetitivo : MonoBehaviour
{
    const int TOTAL_PLAYERS = 2;
    const string PATH_IMGS_CERTAS = "Conscientizacao\\Corretas";
    const string PATH_IMGS_ERRADAS = "Conscientizacao\\Erradas";

    public const string TAG = "GameController";
    public const int TOTAL_QUIZ = 10;

    public static Dictionary<int, int> pontuacoesPlayers = new Dictionary<int, int>(TOTAL_PLAYERS);
    public static List<int> listaFinalizados = new List<int>(TOTAL_PLAYERS);

    private Dictionary<string, Sprite> cacheImagensCertas;
    public Dictionary<string, Sprite> CacheImagensCertas
    {
        get
        {
            if (cacheImagensCertas == null)
            {
                var sprites = Resources.LoadAll<Sprite>(PATH_IMGS_CERTAS);
                cacheImagensCertas = new Dictionary<string, Sprite>(sprites.Length);

                foreach (var sprite in sprites)
                    cacheImagensCertas.Add(sprite.name, sprite);
            }

            return cacheImagensCertas;
        }
    }

    private Dictionary<string, Sprite> cacheImagensErradas;
    public Dictionary<string, Sprite> CacheImagensErradas
    {
        get
        {
            if (cacheImagensErradas == null)
            {
                var sprites = Resources.LoadAll<Sprite>(PATH_IMGS_ERRADAS);
                cacheImagensErradas = new Dictionary<string, Sprite>(sprites.Length);

                foreach (var sprite in sprites)
                    cacheImagensErradas.Add(sprite.name, sprite);
            }

            return cacheImagensErradas;
        }
    }

    public GameObject bSair;
    public GameObject Painel;

    private GameObject[] fundosCache;
    private GameObject[] FundosCache
    {
        get
        {
            if (fundosCache == null)
                fundosCache = GameObject.FindGameObjectsWithTag(GerenciadorPlayer.TAG);

            return fundosCache;
        }
    }

    static System.Random _random = new System.Random();

    public static bool TerminouJogo
    {
        get { return pontuacoesPlayers.Count >= TOTAL_PLAYERS && listaFinalizados.Count >= TOTAL_PLAYERS; }
    }

    void Start()
    {
        SetFundos(false);

        HashSet<string> imgsCorretas = new HashSet<string>();
        HashSet<string> imgsErradas = new HashSet<string>();

        while (imgsCorretas.Count < TOTAL_QUIZ)
        {
            int index = _random.Next(0, CacheImagensCertas.Count);
            imgsCorretas.Add(CacheImagensCertas.Keys.ElementAt(index));
        }

        while (imgsErradas.Count < TOTAL_QUIZ * 3)
        {
            int index = _random.Next(0, CacheImagensErradas.Count);
            imgsErradas.Add(CacheImagensErradas.Keys.ElementAt(index));
        }

        DistribuiImagens(imgsCorretas.ToArray(), imgsErradas.ToArray());
    }

    static string[] RandomizeStrings(string[] arr)
    {
        var list = new List<KeyValuePair<int, string>>(arr.Length);

        foreach (string s in arr)
            list.Add(new KeyValuePair<int, string>(_random.Next(), s));

        return list.OrderBy(x => x.Key).Select(x => x.Value).ToArray();
    }

    private void DistribuiImagens(string[] imgsCorretas, string[] imgsErradas)
    {
        foreach (var objFundo in FundosCache)
        {
            var fundoScript = objFundo.GetComponent<GerenciadorPlayer>();
            if (fundoScript == null)
                continue;

            fundoScript.SetKeys(RandomizeStrings(imgsCorretas), RandomizeStrings(imgsErradas));
            fundoScript.IniciarJogo();
        }
    }

    void Update()
    {
		
	}

    public static void JogadorFinalizou(int idJogador, int pontos)
    {
        listaFinalizados.Add(idJogador);

        if (TerminouJogo)
        {
            //exibe as pontuacoes e finaliza o jogo
            Debug.Log("Fim de Jogo");

            for (int i = 0; i < pontuacoesPlayers.Count; i++)
                Debug.Log(string.Format("Jogador: {0} Pts: {1}", pontuacoesPlayers.Keys.ElementAt(i), pontuacoesPlayers.Values.ElementAt(i)));

            GerenciadorTimer.timerStop();
        }
    }

    public void comecarJogo()
    {
        //Desabilita a tela de instruções
        gameObject.transform.parent.gameObject.SetActive(false);
        bSair.SetActive(true);
        Painel.SetActive(true);
        SetFundos(true);
        GerenciadorTimer.timerStart();
    }

    private void SetFundos(bool habilitar)
    {
        foreach (var fundo in FundosCache)
            fundo.SetActive(habilitar);
    }

    public void TerminaJogos()
    {
        if (TerminouJogo)
            return;

        foreach (var objFundo in FundosCache)
        {
            var fundoScript = objFundo.GetComponent<GerenciadorPlayer>();

            Debug.Log(fundoScript.Terminou);

            if (fundoScript == null || fundoScript.Terminou)
                continue;

            fundoScript.TerminaJogo();
        }

        if (listaFinalizados.Count != TOTAL_PLAYERS)
            listaFinalizados.AddRange(new int[] { 1, 2, 3 });
    }
}
