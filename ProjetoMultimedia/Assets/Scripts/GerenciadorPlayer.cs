﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GerenciadorPlayer : MonoBehaviour
{
    public const string TAG = "Fundo";

    static System.Random _random = new System.Random();

    int qtdAcertos = 0;
    int indexQuiz = 0;

    string[] keysImgsCorretas;
    string[] keysImgsErradas;

    GerenciadorCompetitivo gerJogo;
    GerenciadorCompetitivo GerJogo
    {
        get
        {
            if (gerJogo == null)
                gerJogo = GameObject.FindGameObjectWithTag(GerenciadorCompetitivo.TAG).GetComponent<GerenciadorCompetitivo>();

            return gerJogo;
        }
    }

    public int IdJogador;
    public GameObject spriteFundo;

    public int Acertos
    {
        get { return qtdAcertos; }
    }

    public bool Terminou
    {
        get { return indexQuiz == GerenciadorCompetitivo.TOTAL_QUIZ; }
    }

    public Button EscolhaCorreta;
    public Button EscolhaErrada1;
    public Button EscolhaErrada2;
    public Button EscolhaErrada3;

    void Start()
    {
	}
	
	void Update()
    {
	}

    public void TerminaJogo()
    {
        Debug.Log("terminei");
        indexQuiz = GerenciadorCompetitivo.TOTAL_QUIZ;

        GerenciadorCompetitivo.JogadorFinalizou(IdJogador, qtdAcertos);
    }

    public void SetKeys(string[] keysCorretas, string[] keysErradas)
    {
        keysImgsCorretas = keysCorretas;
        keysImgsErradas = keysErradas;
    }

    public void ProximoQuiz(bool acertou)
    {
        if (acertou)
        {
            qtdAcertos++;
            spriteFundo.GetComponent<SpriteRenderer>().color = Color.green;
            Debug.Log(string.Format("Jogador: {0} Pts: {1}", IdJogador, qtdAcertos));
        }
        else
        {
            spriteFundo.GetComponent<SpriteRenderer>().color = Color.red;
        }

        StartCoroutine(Wait(() => TrocaCor(Color.white)));

        indexQuiz++;
        CarregaQuiz();

        if (Terminou)
        {
            GerenciadorCompetitivo.JogadorFinalizou(IdJogador, qtdAcertos);
            return;
        }
    }

    private void TrocaCor(Color cor)
    {
        spriteFundo.GetComponent<SpriteRenderer>().color = cor;
    }

    private IEnumerator Wait(Action onCompleted)
    {
        yield return new WaitForSeconds(0.3f);

        onCompleted();
    }

    private void CarregaQuiz()
    {
        if (Terminou)
        {
            EscolhaCorreta.gameObject.SetActive(false);
            EscolhaErrada1.gameObject.SetActive(false);
            EscolhaErrada2.gameObject.SetActive(false);
            EscolhaErrada3.gameObject.SetActive(false);
        }
        else
        {
            EscolhaCorreta.GetComponent<Button>().image.sprite = GerJogo.CacheImagensCertas[keysImgsCorretas[indexQuiz]];

            var keysErradas = keysImgsErradas.Skip(indexQuiz * 3).Take(3).ToArray();

            EscolhaErrada1.image.sprite = GerJogo.CacheImagensErradas[keysErradas[0]];
            EscolhaErrada2.image.sprite = GerJogo.CacheImagensErradas[keysErradas[1]];
            EscolhaErrada3.image.sprite = GerJogo.CacheImagensErradas[keysErradas[2]];

            var botoes = new List<Button>()
            {
                EscolhaErrada1,
                EscolhaErrada2,
                EscolhaErrada3,
            };

            int indexCorreta = _random.Next(0, botoes.Count);
            var tempCorreta = botoes[indexCorreta].transform.position;
            var tempErrada = EscolhaCorreta.transform.position;

            botoes[indexCorreta].transform.position = tempErrada;
            EscolhaCorreta.transform.position = tempCorreta;

            lock (GerenciadorCompetitivo.pontuacoesPlayers)
            {
                if (GerenciadorCompetitivo.pontuacoesPlayers.ContainsKey(IdJogador))
                    GerenciadorCompetitivo.pontuacoesPlayers[IdJogador] = qtdAcertos;
                else
                    GerenciadorCompetitivo.pontuacoesPlayers.Add(IdJogador, qtdAcertos);
            }
        }
    }

    public void IniciarJogo()
    {
        CarregaQuiz();
    }
}
