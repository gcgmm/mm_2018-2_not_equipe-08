﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GerenciadorTimer : MonoBehaviour {

    public Text Timer1;
    public Text Timer2;
    static bool timeStop;
    static float startTime;

    GerenciadorCompetitivo gerJogo;
    GerenciadorCompetitivo GerJogo
    {
        get
        {
            if (gerJogo == null)
                gerJogo = GameObject.FindGameObjectWithTag(GerenciadorCompetitivo.TAG).GetComponent<GerenciadorCompetitivo>();

            return gerJogo;
        }
    }

    // Use this for initialization
    void Start()
    {
        timeStop = true;
        startTime = 60;
	}

    public static void timerStart()
    {
        timeStop = false;
    }

    public static void timerStop()
    {
        startTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!timeStop)
        {
            startTime -= Time.deltaTime;
        } 

        int segundos = (int)(startTime % 60);
        int minutos = (int)(startTime / 60);
        if (segundos == 60)
        {
            minutos++;
        }

        if (startTime <= 0 && !timeStop)
        {
            endGame();
            return;
        }

        if (!timeStop && !Timer1.gameObject.activeInHierarchy)
        {
            Timer1.gameObject.SetActive(true);
            Timer2.gameObject.SetActive(true);
        }
        
        if (Timer1.gameObject.activeInHierarchy && Timer2.gameObject.activeInHierarchy && !timeStop)
        {
            Timer1.text = minutos.ToString() + ":" + ((segundos.ToString().Length < 2) ? "0" + segundos.ToString() : segundos.ToString());
            Timer2.text = minutos.ToString() + ":" + ((segundos.ToString().Length < 2) ? "0" + segundos.ToString() : segundos.ToString());
        }


    }

    void endGame()
    {
        timeStop = true;

        GerJogo.TerminaJogos();
        var pontuacoes = GerenciadorCompetitivo.pontuacoesPlayers.Values.ToArray();

        Timer1.text = string.Format("{0}/{1}", pontuacoes[0], GerenciadorCompetitivo.TOTAL_QUIZ);
        Timer2.text = string.Format("{0}/{1}", pontuacoes[1], GerenciadorCompetitivo.TOTAL_QUIZ);
    }
}
