﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscolhaBehaviour : MonoBehaviour
{
    public void enviaResposta()
    {
        var gerEscolhas = GetComponentInParent<GerenciadorEscolhas>();
        gerEscolhas.ValidaEscolha(gameObject);
    }
}
