﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GerenciadorEscolhas : MonoBehaviour
{
    public GameObject EscolhaCorreta;
    public Text timerText;
    public Material fonte;
    public Material padrao;

    private Colisor.LacunaState EstadoAtual = Colisor.LacunaState.SemLacuna;
    private int indexPergunta = 2;
    List<Button> opcoes;
    Dictionary<string, Sprite> spritesPerguntas;
    Dictionary<string, AudioClip> audios;
    Image enunciado;

    const string PATH_SPRITES_PERGUNTAS = "Perguntas";

    void Start()
    {
        spritesPerguntas = new Dictionary<string, Sprite>();
        audios = new Dictionary<string, AudioClip>();

        foreach (var audio in Resources.LoadAll<AudioClip>("Audios"))
            audios.Add(audio.name, audio);

        foreach (var sprite in Resources.LoadAll<Sprite>(PATH_SPRITES_PERGUNTAS))
            spritesPerguntas.Add(sprite.name, sprite);

        opcoes = new List<Button>(4);

        foreach (Transform child in transform)
        {
            var image = child.gameObject.GetComponent<Image>();
            if (image != null)
                enunciado = image;

            var obj = child.gameObject.GetComponent<Button>();
            if (obj == null || obj.GetComponent<EscolhaBehaviour>() == null)
                continue;

            opcoes.Add(obj);
        }
    }

    public void ValidaEscolha(GameObject botao)
    {
        if (EstadoAtual == Colisor.LacunaState.SemLacuna)
        {
            if (botao == EscolhaCorreta)
            {
                EstadoAtual = Colisor.LacunaState.LacunaCorreta;
                Timer.qtdeAcertosP++;
            }
            else
                EstadoAtual = Colisor.LacunaState.LacunaIncorreta;

            TrataEstadoAtual(botao);
        }
    }

    private void TrataEstadoAtual(GameObject botao)
    {
        //Tratar aqui para alternar entre as perguntas
        switch (EstadoAtual)
        {
            case Colisor.LacunaState.LacunaIncorreta:
                {
                    Debug.Log("Escolha incorreta");

                    Image imgBtn = botao.GetComponent<Image>();
                    imgBtn.color = Color.red;
                    imgBtn.material = fonte;

                    imgBtn = EscolhaCorreta.GetComponent<Image>();
                    imgBtn.color = Color.green;
                    imgBtn.material = fonte;
                    
                    GetComponent<AudioSource>().PlayOneShot(audios["SomErrou"]);

                }
                break;

            case Colisor.LacunaState.LacunaCorreta:
                {
                    Debug.Log("Escolha correta");
                    
                    Image imgBtn = botao.GetComponent<Image>();
                    imgBtn.color = Color.green;
                    imgBtn.material = fonte;
                   
                    GetComponent<AudioSource>().PlayOneShot(audios["SomAcertou"]);
                }
                break;

            default:
                Debug.Log("Sem escolha");
                break;
        }

        TrocaPergunta();
    }

    private void TrocaPergunta()
    {
        StartCoroutine(Sleep(InternalTrocaPergunta));
    }

    IEnumerator Sleep(Action onCompleted)
    {
        yield return new WaitForSeconds(2);

        onCompleted();
    }

    private void InternalTrocaPergunta()
    {
        //A partir desta linha todos as telas filhas do Foco ficam invisíveis
        transform.parent.gameObject.SetActive(false);
        Timer timer = timerText.GetComponent<Timer>();
        timer.playTimer();

        AtualizaTelaPergunta();
    }

    private void AtualizaTelaPergunta()
    {
        foreach (Button botao in opcoes)
        {
            Image imgBtn = botao.GetComponent<Image>();
            imgBtn.color = Color.white;
            imgBtn.material = padrao;
        }

        EstadoAtual = Colisor.LacunaState.SemLacuna;
        
        switch (indexPergunta)
        {
            case 1:
                {
                    enunciado.sprite = spritesPerguntas[string.Format("P{0}", indexPergunta)];

                    opcoes[0].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 2)];
                    opcoes[1].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 3)];
                    opcoes[2].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 1)];
                    opcoes[3].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 4)];

                    EscolhaCorreta = opcoes[2].gameObject;
                    indexPergunta++;
                }
                break;

            case 2:
                {
                    enunciado.sprite = spritesPerguntas[string.Format("P{0}", indexPergunta)];

                    opcoes[0].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 2)];
                    opcoes[1].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 3)];
                    opcoes[2].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 1)];
                    opcoes[3].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 4)];

                    EscolhaCorreta = opcoes[1].gameObject;
                    indexPergunta++;
                }
                break;

            case 3:
                {
                    enunciado.sprite = spritesPerguntas[string.Format("P{0}", indexPergunta)];

                    opcoes[0].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 2)];
                    opcoes[1].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 3)];
                    opcoes[2].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 1)];
                    opcoes[3].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 4)];

                    EscolhaCorreta = opcoes[0].gameObject;
                    indexPergunta++;
                }
                break;

            default:
                {
                    enunciado.sprite = spritesPerguntas[string.Format("P{0}", indexPergunta)];

                    opcoes[0].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 2)];
                    opcoes[1].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 3)];
                    opcoes[2].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 1)];
                    opcoes[3].image.sprite = spritesPerguntas[string.Format("P{0}R{1}", indexPergunta, 4)];

                    EscolhaCorreta = opcoes[1].gameObject;
                    indexPergunta = 1;
                }
                break;
        }
    }
}
