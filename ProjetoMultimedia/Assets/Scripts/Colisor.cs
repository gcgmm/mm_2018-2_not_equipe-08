﻿using System;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;

public class Colisor : MonoBehaviour
{
    const string TAG_LACUNA = "Lacuna";

    public enum LacunaState
    {
        SemLacuna,
        LacunaIncorreta,
        LacunaCorreta,
    }

    public GameObject LacunaAlvo;

    private LacunaState estadoAtual = LacunaState.SemLacuna;

    private int qtdLacunas = 0;

    private GerenciadorJogo gerJogo;

    void TrocaEstado(LacunaState state)
    {
        LacunaState estadoAnterior = estadoAtual;
        estadoAtual = state;

        Debug.Log(string.Format("{0} trocou do estado {1} para {2}", name, estadoAnterior, estadoAtual));
    }

    public Material fonte;
    public Material padrao;

    void TrocaCorFundo(bool mostrarResultado)
    {
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();

        if (!mostrarResultado)
        {
            sprite.material = padrao;
            sprite.color = Color.white;
            return;
        }

        gerJogo.IncrementaAcessos();
        switch (estadoAtual)
        {
            case LacunaState.LacunaIncorreta:
                sprite.material = fonte;
                sprite.color = Color.red;
                break;

            case LacunaState.LacunaCorreta:
                {
                    sprite.material = fonte;
                    sprite.color = Color.green;

                    TransformGesture touch = GetComponent<TransformGesture>();

                    //Contabiliza acerto apenas uma vez por lacuna
                    if (touch.Type != TransformGesture.TransformType.None)
                        gerJogo.IncrementaAcertos();
                    

                    touch.Type = TransformGesture.TransformType.None;
                }
                break;

            default:
                sprite.material = padrao;
                sprite.color = Color.white;
                break;
        }
    }

    private void MostrarResultado(bool podeMostrar)
    {
        Debug.Log("MostrarResultado");
        TrocaCorFundo(podeMostrar);
    }

    #region MÉTODOS DO UNITY

    void Start()
    {
        gerJogo = GameObject.FindWithTag(GerenciadorJogo.TAG_FUNDO).GetComponent(GerenciadorJogo.SCRIPT_GER_JOGO) as GerenciadorJogo;

        gerJogo.OnTrocouMostrarResultado += MostrarResultado;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        var outroObj = coll.gameObject;

        if (outroObj.CompareTag(TAG_LACUNA))
        {
            if (outroObj.name == LacunaAlvo.name)
                TrocaEstado(LacunaState.LacunaCorreta);
            else
                TrocaEstado(LacunaState.LacunaIncorreta);

            qtdLacunas++;
        }

        //Debug.Log(string.Format("{0} colidiu com {1}", name, coll.gameObject.name));
    }

    void OnTriggerStay2D(Collider2D coll)
    {

        //Debug.Log(string.Format("{0} está colidindo com {1}", name, coll.gameObject.name));
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        var outroObj = coll.gameObject;

        if (outroObj.CompareTag(TAG_LACUNA))
        {
            qtdLacunas--;

            if (qtdLacunas == 0)
                TrocaEstado(LacunaState.SemLacuna);
        }

        //Debug.Log(string.Format("{0} parou de colidir com {1}", name, coll.gameObject.name));
    }

    #endregion
}
