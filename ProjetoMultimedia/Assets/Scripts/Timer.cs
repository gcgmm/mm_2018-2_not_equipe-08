﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    // Use this for initialization
    Dictionary<string, AudioClip> audios;
    public GerenciadorJogo gerJogo;
    public Text timerText;
    public float startTime;
    public float ultimaPergunta;
    public GameObject focoPanel;
    public GameObject fimPanel;
    bool timeStop;
    bool musicaFim;
    int qtdePerguntas;
    public static int qtdeAcertosP;

	void Start () {
        gerJogo = GameObject.FindWithTag(GerenciadorJogo.TAG_FUNDO).GetComponent(GerenciadorJogo.SCRIPT_GER_JOGO) as GerenciadorJogo;
        audios = new Dictionary<string, AudioClip>();
        foreach (var audio in Resources.LoadAll<AudioClip>("Audios"))
            audios.Add(audio.name, audio);
        fimPanel.SetActive(false);
        timeStop = false;
        musicaFim = false;
        qtdePerguntas = 0;
        qtdeAcertosP = 0;
        startTime = ultimaPergunta = 299;
    }
	
	void Update () {

        if((ultimaPergunta - startTime) >= 60)
        {
            Debug.Log("Lança pergunta");
            ultimaPergunta = startTime;
            timeStop = true;
            focoPanel.SetActive(true);
            qtdePerguntas++;

        }

        if (!timeStop)
        {
            startTime -= Time.deltaTime;
        }
        
        int segundos = (int) (startTime % 60);
        int minutos = (int)(startTime / 60);
        if (segundos == 60)
        {
            minutos++;
        }

    
        if(startTime <= 0 && !timeStop)
        {   
            endGame();
            return;
        }

        timerText.text = minutos.ToString() + ":" + ((segundos.ToString().Length < 2) ? "0"+ segundos.ToString() : segundos.ToString());

	}

    public void stopTimer()
    {
        timeStop = true;
    }

    public void endGame(){

        stopTimer();
        
        if(!musicaFim){
            playEndMusic();
            setFimPanelStuff();
            musicaFim = true;
        }
    }

    public void playTimer()
    {
        timeStop = false;
    }

    void setFimPanelStuff()
    {

        fimPanel.SetActive(true);
        GameObject.Find("FundoFim/AuxPont").GetComponent<Text>().text = (gerJogo.pontuacaoFinal != -1 ? gerJogo.pontuacaoFinal.ToString() : "0") + "/" + GerenciadorJogo.TOTAL_LACUNAS.ToString();
        GameObject.Find("FundoFim/AuxTempo").GetComponent<Text>().text = timerText.text;
        GameObject.Find("FundoFim/AuxPergu").GetComponent<Text>().text = qtdeAcertosP.ToString() + "/" + qtdePerguntas.ToString();

    }

    void playEndMusic(){
        GetComponent<AudioSource>().PlayOneShot(audios["SomFim"]);
    }
}
